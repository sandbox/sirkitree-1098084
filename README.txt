Stager.sh is a little shell script that eases the headache of keeping a staging
server in sync with development changes, and also the production site. It 
currently only works with git repos.

# Requirements

- Drush 3.x
- Git
- Ruby (rb is used only to parse urls, so if you have another scripting language
you'd like to use to parse urls, hack away!)
- Open SSH
- SFTP

# Usage

Usage: ./stager.sh [ -h ] [ -f ] [ -d ]
  [ -s sites/stagingsite.com/files ]
  [ -p sites/prodsite.com/files ]
  [http://username@server.com]/path/to/drupal#stagingsite.com
  [http://username@server.com]/path/to/drupal#prodsite.com

  -f Flag to specify to rsync the files dir. SSH *must* connect via Public
     Key Authentication for this to work.
  -d USE WITH CAUTION! Flag to specify to copy the production database to 
     the staging database.
  -p The relative path to the Production site's files dir.
  -s The relative path to the Staging site's files dir.