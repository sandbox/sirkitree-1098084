#! /bin/sh

########################################################################
# Staging Server Sync
#
# If in an Aegir env., this script should be run as the Aegir user.

# Some basic functions
# Noticeable messages
msg() {
  echo "==> $*"
}

# Simple prompt
prompt_yes_no() {
  while true ; do
    printf "$* [Y/n] "
    read answer
    if [ -z "$answer" ] ; then
      return 0
    fi
    case $answer in
      [Yy]|[Yy][Ee][Ss])
        return 0
        ;;
      [Nn]|[Nn][Oo])
        return 1
        ;;
      *)
        echo "Please answer yes or no"
        ;;
    esac
 done 

}

# Help
usage() {
  cat <<EOF
Usage: $0 [ -h ] [ -y ] [ @site-alias ]
  -y Answer yes to all prompts.
EOF
}

########################################################################
# Main script
########################################################################

# Stop on error
set -e

# Parse commands
args=`getopt yh $*`
set -- $args

for i
do
  case "$i" in
    -y) NO_PROMPT=1; shift;;
    -h) shift; usage; exit;;
    --) shift; break;;
  esac
done

ALIAS=${1}
MESSAGE="Are you sure you want to update $ALIAS? The git repo will be updated, update.php will be run, and all features reverted. There is no undo!"

if [ ! $NO_PROMPT ]; then
  if prompt_yes_no $MESSAGE; then
    true
  else
    echo "Script was aborted."
    exit 1
  fi
fi

if [ ! -z $ALIAS ]; then
  drush $ALIAS gittyup --no-rebase
  drush $ALIAS updb --yes
  drush $ALIAS fra --yes
  drush $ALIAS cc all --yes
else
  echo "You need to specify a site alias."
  exit 1
fi